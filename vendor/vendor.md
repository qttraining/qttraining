Third party libraries
=====================

## SOAP

Qt do not provide library to directly working with SOAP. We can write SOAP wrapper with `QNetworkAccessManager` family classes and Qt `XML` classes, but we will not do this. For our purpose, we will use open source third party library [KD Soap](https://github.com/KDAB/KDSoap). KD Soap is a Qt-based client-side and server-side SOAP component.

Other alternatives 

* [QtSOAP](https://qt.gitorious.org/qt-solutions/qt-solutions)
* [gSOAP](http://www.cs.fsu.edu/~engelen/soap.html)

QtSOAP is very simple and gSOAP is not really Qt. 

### Steps to Install KD Soap

Clone the repo
```
  git clone https://github.com/KDAB/KDSoap.git
```
 
Cd into folder
```
 cd KDSoap
```

Update submodule
```
  git submodule update --init
```

Set Visual Studio environment in command line
```
  %comspec% /k ""c:\Program Files\Microsoft Visual Studio 9.0\VC\vcvarsall.bat"" x86
```

Set Qt in path
```
  set path=%path%;C:\QtSDK\Desktop\Qt\4.8.1\msvc2008\bin
```

Install python then one of the followings in Visual Studio environment with Qt in path (which we set above)

```
  python autogen.py -shared -debug
  python autogen.py -shared -release
  python autogen.py -static -debug
  python autogen.py -static -release
```
 
If everything went OK, you'll have folders `lib`, `bin` and `include` ready for use

See examples to get started

Included in this distribution Visual Studio 2008 prebuild libs release version 
