TEMPLATE = subdirs
INSTALL_HEADERS.files = KDSoapDelayedResponseHandle \
KDSoapDelayedResponseHandle.h \
KDSoapServer \
KDSoapServer.h \
KDSoapServerAuthInterface \
KDSoapServerAuthInterface.h \
KDSoapServerGlobal \
KDSoapServerGlobal.h \
KDSoapServerObjectInterface \
KDSoapServerObjectInterface.h \
KDSoapThreadPool \
KDSoapThreadPool.h \
KDSoapServer \

INSTALL_HEADERS.path = $$INSTALL_PREFIX/include/KDSoapServer
INSTALLS += INSTALL_HEADERS
