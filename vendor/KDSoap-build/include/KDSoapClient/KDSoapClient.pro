TEMPLATE = subdirs
INSTALL_HEADERS.files = KDDateTime \
KDDateTime.h \
KDSoap \
KDSoap.h \
KDSoapAuthentication \
KDSoapAuthentication.h \
KDSoapClientInterface \
KDSoapClientInterface.h \
KDSoapGlobal \
KDSoapGlobal.h \
KDSoapJob \
KDSoapJob.h \
KDSoapMessage \
KDSoapHeaders \
KDSoapMessage.h \
KDSoapNamespaceManager \
KDSoapNamespaceManager.h \
KDSoapPendingCall \
KDSoapPendingCall.h \
KDSoapPendingCallWatcher \
KDSoapPendingCallWatcher.h \
KDSoapSslHandler \
KDSoapSslHandler.h \
KDSoapValue \
KDSoapValueList \
KDSoapValue.h \
KDSoapClient \

INSTALL_HEADERS.path = $$INSTALL_PREFIX/include/KDSoapClient
INSTALLS += INSTALL_HEADERS
