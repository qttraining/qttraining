#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>

#include "object.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)),
            this, SLOT(onItemClicked(QListWidgetItem*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // static object creation
    Object a;
    a.setData("This is string data");

    qDebug() << a.data();
}

void MainWindow::on_pushButton_2_clicked()
{
    // dynamic object
    Object * b = new Object();
    b->setData("B data");

    qDebug() << b->data();

    delete b;
}

void MainWindow::on_pushButton_3_clicked()
{
    // dynamic object creation with parent
    Object * b = new Object(this);
    b->setData("B data");

    qDebug() << b->data();
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->listWidget->addItem("This is item no " + QString::number(ui->listWidget->count()));
}

void MainWindow::onItemClicked(QListWidgetItem *item)
{
    qDebug() << "Item Clicked!!" << item->text();
    //item->setText("this is ok");

    QMessageBox msgBox;
    QPushButton * okBtn = msgBox.addButton(QMessageBox::Ok);
    okBtn->setProperty("idx", ui->listWidget->row(item));
    QPushButton * cancelBtn = msgBox.addButton(QMessageBox::Cancel);
    msgBox.setText("Delete item " + item->text() + "?");

    connect(okBtn, SIGNAL(clicked()), this, SLOT(onButtonOK()));
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(onButtonCancel()));

    msgBox.exec();
}

void MainWindow::onButtonOK()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    int idx = btn->property("idx").toInt();

    delete ui->listWidget->item(idx);
    //QListWidgetItem *item = ui->listWidget->takeItem(idx);
    qDebug() << "Remove Item index: " << idx;
    //delete item;
}

void MainWindow::onButtonCancel()
{
    qDebug() << "Cancel";
}
