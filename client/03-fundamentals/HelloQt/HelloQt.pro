#-------------------------------------------------
#
# Project created by QtCreator 2013-11-25T10:22:42
#
#-------------------------------------------------

QT       += core gui

TARGET = HelloQt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    object.cpp

HEADERS  += mainwindow.h \
    object.h

FORMS    += mainwindow.ui
