#include "object.h"

#include <QDebug>

Object::Object(QObject *parent) :
    QObject(parent)
{
    qDebug() << "class created";
    obj = new QObject();
}

Object::~Object()
{
    qDebug() << "class deleted";
    delete obj;
}

void Object::setData(const QString &data)
{
    m_data = data;
}

QString Object::data()
{
    return m_data;
}

QString Object::encrypt(QString &data)
{
    QString encrypted = data.replace("a","c");
    return encrypted;
}
