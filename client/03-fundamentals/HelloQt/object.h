#ifndef OBJECT_H
#define OBJECT_H

#include <QObject>

// class name: Object
class Object : public QObject
{
    Q_OBJECT
public:
    explicit Object(QObject *parent = 0);
    ~Object();
    
    // functions
    // setter
    void setData(const QString &data);

    // getter
    QString data();

    // another method
    QString encrypt(QString &data);

private: // all things below is private
    QString m_data;

    QObject * obj;
};

#endif // OBJECT_H
