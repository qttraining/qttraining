#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "object.h"

#include <QDebug>
#include <QString>
#include <QStringList>
#include <QMetaObject>
#include <QMetaMethod>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    createObject();
    knowObject();
    propertyObject();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createObject()
{
    Object * a = new Object("a", this);
    a->setNumber(2);
}

void MainWindow::knowObject()
{
    Object * b = new Object("b", this);
    qDebug() << "name" << QString::fromAscii(b->metaObject()->className());

    const QMetaObject* metaObject = b->metaObject();
    QStringList methods;
    for(int i = metaObject->methodOffset(); i < metaObject->methodCount(); ++i)
        methods << QString::fromLatin1(metaObject->method(i).signature());

    qDebug() << "methods" << methods;

    int index = metaObject->indexOfEnumerator("TypeEnum");
    QMetaEnum metaEnum = metaObject->enumerator(index);
    qDebug() << "enum" << metaEnum.valueToKey(Object::Boolean);

    b->setNumber(24);
    int count = metaObject->propertyCount();
    for (int i=0; i<count; ++i) {
        QMetaProperty metaProperty = metaObject->property(i);
        const char *name = metaProperty.name();
        QVariant value = b->property(name);
        qDebug() << name << value;
    }
}

void MainWindow::propertyObject()
{
    Object * c = new Object("c", this);
    c->setProperty("number", 13);
    c->setProperty("one", "23");

    qDebug() << c->number();
    qDebug() << c->property("one");

    const QMetaObject* metaObject = c->metaObject();
    int count = metaObject->propertyCount();
    for (int i=0; i<count; ++i) {
        QMetaProperty metaProperty = metaObject->property(i);
        const char *name = metaProperty.name();
        QVariant value = c->property(name);
        qDebug() << name << value;
    }
}
