#include "object.h"
#include <QDebug>

Object::Object(QString name, QObject *parent) :
    QObject(parent)
{
    m_name = name;
    qDebug() << m_name << "object created";
}

Object::~Object()
{
    qDebug() << m_name << "object deleted";
}

int Object::number()
{
    return m_number;
}

void Object::setNumber(int i)
{
    m_number = i;
}
