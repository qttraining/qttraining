#-------------------------------------------------
#
# Project created by QtCreator 2013-11-20T14:50:37
#
#-------------------------------------------------

QT       += core gui

TARGET = QObject
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    object.cpp

HEADERS  += mainwindow.h \
    object.h

FORMS    += mainwindow.ui
