#ifndef OBJECT_H
#define OBJECT_H

#include <QObject>

class Object : public QObject
{
    Q_OBJECT
    Q_ENUMS(TypeEnum)
    Q_PROPERTY(int number READ number WRITE setNumber)

public:
    enum TypeEnum { String, Numeric, Boolean, Array };

public:
    explicit Object(QString name, QObject *parent = 0);
    virtual ~Object();
    
public slots:
    int number();
    void setNumber(int i);
    
private:
    int m_number;
    QString m_name;
};

#endif // OBJECT_H
