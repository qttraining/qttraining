// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import QtDesktop 0.1

Rectangle {
    width: 360
    height: 360

    Text {
        id: text
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }

    Button {
        text: "Click";
        anchors.top: text.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 10
        onClicked: {
            console.log('button clicked');
            text.text = "Clicked";
        }
    }

    /*
    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }
    */
}
