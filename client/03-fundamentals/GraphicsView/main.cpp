#include <QtGui/QApplication>
#include <QApplication>
#include <QGraphicsEllipseItem>
#include <QGraphicsScene>
#include <QGraphicsView>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QGraphicsScene scene;
    scene.setSceneRect( -100.0, -100.0, 200.0, 200.0 );
    scene.setBackgroundBrush(Qt::cyan);

    QGraphicsEllipseItem *item = new QGraphicsEllipseItem( 0, &scene );
    item->setRect( -50.0, -50.0, 100.0, 100.0 );
    item->setBrush( Qt::green );

    QGraphicsView view( &scene );
    view.setRenderHints( QPainter::Antialiasing );
    view.show();

    return a.exec();
}
