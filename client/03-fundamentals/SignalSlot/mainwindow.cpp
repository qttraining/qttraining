#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->listWidget, SIGNAL(pressed(QModelIndex)), this, SLOT(onListItemPressed(QModelIndex)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->label->setText("You clicked!");
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->listWidget->addItem("New Item " + QString::number(ui->listWidget->count()));
}

void MainWindow::onListItemPressed(QModelIndex idx)
{
    qDebug() << "Item" << idx.row() << "clicked";
    ui->label_2->setText("Item " + QString::number(idx.row()) + " clicked");
}

