#-------------------------------------------------
#
# Project created by QtCreator 2013-11-20T15:56:50
#
#-------------------------------------------------

QT       += core gui

TARGET = SignalSlot
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
