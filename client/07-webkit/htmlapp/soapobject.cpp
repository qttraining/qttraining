#include "soapobject.h"

#include <QDebug>

SOAPObject::SOAPObject(QObject *parent) :
    QObject(parent)
{
}

void SOAPObject::sayHello()
{
    qDebug() << "say hello from HTML to SOAPObject";
}

void SOAPObject::login(const QString &username, const QString &password)
{
    qDebug() << "username: " << username;
    qDebug() << "password: " << password;

    // TODO: call SOAP plugin getToken(username, password)
}
