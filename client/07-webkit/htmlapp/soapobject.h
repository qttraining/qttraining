#ifndef SOAPOBJECT_H
#define SOAPOBJECT_H

#include <QObject>

class SOAPObject : public QObject
{
    Q_OBJECT
public:
    explicit SOAPObject(QObject *parent = 0);
    
signals:
    
public slots:
    void sayHello();
    void login(const QString & username, const QString & password);
    
};

#endif // SOAPOBJECT_H
