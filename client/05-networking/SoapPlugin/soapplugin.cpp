#include "soapplugin.h"

#include <QDebug>

SoapPlugin::SoapPlugin(QObject *parent) :
    SoapClientInterface(parent)
{
    m_service.setEndPoint(QLatin1String("http://jaegers.azurewebsites.net/Service.asmx"));
    m_service.setSoapVersion(KDSoapClientInterface::SOAP1_2);

    connect(&m_service, SIGNAL(tokenDone(TNS__TokenResponse)),
            this, SLOT(onTokenDone(TNS__TokenResponse)));
    connect(&m_service, SIGNAL(tokenError(KDSoapMessage)),
            this, SLOT(onTokenError(KDSoapMessage)));
    connect(&m_service, SIGNAL(downloadDone(TNS__DownloadResponse)),
            this, SLOT(onDownloadDone(TNS__DownloadResponse)));
    connect(&m_service, SIGNAL(downloadError(KDSoapMessage)),
            this, SLOT(onDownloadError(KDSoapMessage)));
}

Q_EXPORT_PLUGIN2(SoapPlugin, SoapPlugin)

void SoapPlugin::getToken(const QString &name, const QString &password)
{
    TNS__Token tokenData;
    tokenData.setName(name);
    tokenData.setPassword(password);
    m_service.asyncToken(tokenData);
}

void SoapPlugin::getDownloadList(const QString &token)
{
    TNS__Download downloadData;
    downloadData.setToken(token);
    m_service.asyncDownload(downloadData);
}

void SoapPlugin::onTokenDone(const TNS__TokenResponse &parameters)
{
    qDebug() << "OtherSoapClient::onTokenDone" << parameters.tokenResult();
    emit tokenObtained(parameters.tokenResult());
}

void SoapPlugin::onTokenError(const KDSoapMessage &fault)
{
    emit error(fault.faultAsString());
}

void SoapPlugin::onDownloadDone(const TNS__DownloadResponse &parameters)
{
    QList<DownloadItem> downloadList;

    QList<TNS__DownloadItem> responseList = parameters.downloadResult().downloadItem();
    for (int i = 0; i < responseList.size(); i++)
    {
        DownloadItem item;
        item.name = responseList[i].name();
        item.description = responseList[i].description();
        item.url = responseList[i].url();
        downloadList.append(item);
    }

    emit listObtained(downloadList);
}

void SoapPlugin::onDownloadError(const KDSoapMessage &fault)
{
    emit error(fault.faultAsString());
}
