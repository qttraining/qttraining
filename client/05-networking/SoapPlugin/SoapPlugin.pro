#-------------------------------------------------
#
# Project created by QtCreator 2013-11-28T09:13:22
#
#-------------------------------------------------

QT       += network

QT       -= gui

include(kdsoap.pri)

WSDL_DIR = generated

KDWSDL = Service.wsdl

TARGET = SoapPlugin
TEMPLATE = lib

DEFINES += SOAPPLUGIN_LIBRARY

SOURCES += soapplugin.cpp

HEADERS += soapplugin.h\
        SoapPlugin_global.h \
    ../HelloSOAP/SoapClientInterface.h

