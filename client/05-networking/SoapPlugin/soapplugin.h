#ifndef SOAPPLUGIN_H
#define SOAPPLUGIN_H

#include <QtPlugin>
#include "../HelloSOAP/SoapClientInterface.h"

// 1
// Include generated wsdl file
#include "wsdl_Service.h"

// 2
// Add using namespace Service
using namespace Service;

class SoapPlugin : public SoapClientInterface
{
    Q_OBJECT
    Q_INTERFACES(SoapClientInterface)

public:
    // both functions and other data type
    explicit SoapPlugin(QObject *parent = 0);

    // this class methods
    void getToken(const QString &name, const QString &password);
    void getDownloadList(const QString &token);

    // use signals from interface

private slots:
    // functions we will use to listen signals from SoapService
    void onTokenDone( const TNS__TokenResponse& parameters );
    void onTokenError( const KDSoapMessage& fault );
    void onDownloadDone( const TNS__DownloadResponse& parameters );
    void onDownloadError( const KDSoapMessage& fault );

private:
    // class we used to do its job
    ServiceSoap m_service;
};

#endif // SOAPPLUGIN_H
