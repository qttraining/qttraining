# Add more folders to ship with the application, here
folder_01.source = qml/ClientApp
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

include(kdsoap.pri)

WSDL_DIR = generated

KDWSDL = WebServiceDemo.wsdl

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

include(core/core.pri)

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()
