import QtQuick 1.1

FocusScope {
    id: loginName
    width: inputRect.width
    height: inputRect.height

    property alias input: input
    property alias placeholder: prompt.text
    property alias text: input.text

    Rectangle {
        id: inputRect
        width: 120
        height: 32
        color: '#0D56A6'
    }

    Rectangle {
        id: inputRectInside
        anchors.fill: inputRect
        anchors.margins: 2
    }

    Text {
        id: prompt
        anchors.fill: inputRectInside
        anchors.margins: 2
        font.pixelSize: 16
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: ''
        visible: input.text == ''
        color: 'gray'
    }

    TextInput {
        id: input
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: inputRectInside
        anchors.margins: 2
        font.pixelSize: 16
    }
}
