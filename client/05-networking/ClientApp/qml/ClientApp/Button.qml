import QtQuick 1.1

Rectangle {
    id: root
    width: 120
    height: 32
    color: mouse.pressed ? "#04356C" : "#4186D3"

    signal clicked
    property alias text: text.text

    Text {
        id: text
        anchors.fill: parent
        anchors.margins: 2
        font.pixelSize: 16
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: {
            root.clicked();
        }
    }
}
