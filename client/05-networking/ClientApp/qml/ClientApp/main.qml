import QtQuick 1.1

Rectangle {
    width: 800
    height: 400
    color: '#689AD3'

    Rectangle {
        id: loginArea
        color: '#FFA473'
        width: 400
        height: 300
        anchors.centerIn: parent

        Column {
            width: 120
            height: 120
            anchors.centerIn: parent
            spacing: 10

            TextField {
                id: name
                placeholder: 'your name'
            }

            TextField {
                id: password
                placeholder: 'your password'
                input.echoMode: TextInput.Password
            }

            Button {
                text: 'Login'
                onClicked: {
                    SoapClient.getToken(name.text, password.text);
                }
            }
        }
    }
}
