#include <QtGui/QApplication>
#include <QDeclarativeContext>

#include "qmlapplicationviewer.h"
#include "soapclient.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    SoapClient soapClient;
    soapClient.initialize();

    QmlApplicationViewer viewer;

    QDeclarativeContext * context = viewer.rootContext();
    context->setContextProperty("SoapClient", &soapClient);

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/ClientApp/main.qml"));
    viewer.showExpanded();

    return app->exec();
}
