#ifndef SOAPCLIENT_H
#define SOAPCLIENT_H

#include <QObject>
#include "wsdl_WebServiceDemo.h"

using namespace Service;

class SoapClient : public QObject
{
    Q_OBJECT

public:
    explicit SoapClient(QObject *parent = 0);

    void initialize();

signals:
    void tokenDone(const QString &token);
    void soapError(const QString &source, const QString &message);
    
public slots:
    void getToken( const QByteArray &name, const QByteArray & password);

private slots:
    void onTokenDone( const TNS__TokenResponse& parameters );
    void onSoapError( const KDSoapMessage& fault );
    
private:
    ServiceSoap m_service;
};

#endif // SOAPCLIENT_H
