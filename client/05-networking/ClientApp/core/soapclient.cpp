#include "soapclient.h"
#include <QDebug>

SoapClient::SoapClient(QObject *parent) :
    QObject(parent)
{
}

void SoapClient::getToken(const QByteArray &name, const QByteArray &password)
{
    TNS__Token tokenData;
    tokenData.setName(name);
    tokenData.setPassword(password);
    m_service.asyncToken(tokenData);
}

void SoapClient::initialize()
{
    m_service.setEndPoint(QLatin1String("http://jaegers.azurewebsites.net/Service.asmx"));
    m_service.setSoapVersion(KDSoapClientInterface::SOAP1_2);
    connect(&m_service, SIGNAL(tokenDone(TNS__TokenResponse)), this, SLOT(onTokenDone(TNS__TokenResponse)));
    connect(&m_service, SIGNAL(tokenError(KDSoapMessage)), this, SLOT(onSoapError(KDSoapMessage)));
}

void SoapClient::onTokenDone( const TNS__TokenResponse& parameters )
{
    qDebug() << "onTokenDone" << parameters.tokenResult();
    emit tokenDone(parameters.tokenResult());
}

void SoapClient::onSoapError( const KDSoapMessage& fault )
{
    KDSoapValueList arg = fault.arguments();
    KDSoapValue error = arg.child("detail").childValues().child("Error");
    QString message = error.childValues().child("ErrorMessage").value().toString();
    QString source = error.childValues().child("ErrorSource").value().toString();

    qDebug() << "onSoapError" << source << message;
    emit soapError(source, message);
}
