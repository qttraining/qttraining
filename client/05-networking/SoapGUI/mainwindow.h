#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "SoapClientInterface.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    void loadPlugin();

private slots:
    void on_pushButton_clicked();

    void onTokenObtained(const QString &token);
    void onListObtained(const QList<DownloadItem> &downloadList);
    void onError(const QString &token);

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;

    SoapClientInterface * soapClient;
    QString m_token;
};

#endif // MAINWINDOW_H
