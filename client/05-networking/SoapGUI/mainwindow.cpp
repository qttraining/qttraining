#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QPluginLoader>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    loadPlugin();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadPlugin()
{
    QPluginLoader pluginLoader;
    pluginLoader.setFileName("SoapPlugin.dll");
    bool ok = pluginLoader.load();
    if (ok) {
        qDebug() << "Plugin loaded";
        QObject *plugin = pluginLoader.instance();
        SoapClientInterface * client = qobject_cast<SoapClientInterface *>(plugin);
        if (client) {
            // set our soapClient
            soapClient = client;

            // connect signal from plugin to ui slot
            connect(soapClient, SIGNAL(tokenObtained(QString)),
                    this, SLOT(onTokenObtained(QString)));
            connect(soapClient, SIGNAL(listObtained(QList<DownloadItem>)),
                    this, SLOT(onListObtained(QList<DownloadItem>)));
            connect(soapClient, SIGNAL(error(QString)),
                    this, SLOT(onError(QString)));
        }
    }
    else {
        qDebug() << "Plugin load failed" << pluginLoader.errorString();
    }
}

void MainWindow::on_pushButton_clicked()
{
    QString name = ui->lineEditName->text();
    QString password = ui->lineEditPassword->text();
    soapClient->getToken(name, password);
}

void MainWindow::onTokenObtained(const QString &token)
{
    qDebug() << "MainWindow::onTokenObtained" << token;
    m_token = token;
    ui->label->setText(token);
}

void MainWindow::onListObtained(const QList<DownloadItem> &downloadList)
{
    for (int i = 0; i < downloadList.count(); i++) {
        ui->listWidget->addItem(downloadList[i].url);
    }
}

void MainWindow::onError(const QString &token)
{

}

void MainWindow::on_pushButton_2_clicked()
{
    soapClient->getDownloadList(m_token);
}
