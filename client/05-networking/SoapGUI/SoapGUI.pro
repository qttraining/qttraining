#-------------------------------------------------
#
# Project created by QtCreator 2013-11-28T10:19:40
#
#-------------------------------------------------

QT       += core gui

TARGET = SoapGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    SoapClientInterface.h

FORMS    += mainwindow.ui
