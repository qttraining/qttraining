#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "othersoapclient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_pushButton_clicked();
    void on_pushButtonDownload_clicked();

    // slots untuk menerima sinyal
    void onTokenObtained(const QString &token);
    void onListObtained(const QList<TNS__DownloadItem> &downloadList);
    void onError(const QString &token);

private:
    Ui::MainWindow *ui;

    // instance
    OtherSoapClient m_soapClient;
    QString m_token;
};

#endif // MAINWINDOW_H
