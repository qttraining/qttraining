#ifndef SOAPSERVICE_H
#define SOAPSERVICE_H

#include <QObject>

// 1
// Include generated wsdl file
#include "wsdl_Service.h"

// 2
// Add using namespace Service
using namespace Service;

class SoapService : public QObject
{
    Q_OBJECT
public:
    explicit SoapService(QObject *parent = 0);
    
public:
    void initialize();
    void getToken(const QString &name, const QString &password);

signals:
    void tokenDone(const QString &token);
    void error(const QString &message);
    
private slots:
    // 4
    // Add slot for signals from get Token
    void onTokenDone( const TNS__TokenResponse& parameters );
    void onTokenError( const KDSoapMessage& fault );

private:
    // 3
    // Add Service variable
    ServiceSoap m_service;
};

#endif // SOAPSERVICE_H
