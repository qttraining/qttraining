#include "soapservice.h"

#include <QDebug>

SoapService::SoapService(QObject *parent) :
    QObject(parent)
{
}

void SoapService::initialize()
{
    // 5
    // initialize service
    m_service.setEndPoint(QLatin1String("http://jaegers.azurewebsites.net/Service.asmx"));
    m_service.setSoapVersion(KDSoapClientInterface::SOAP1_2);

    // 6
    // connect signal and slots
    connect(&m_service, SIGNAL(tokenDone(TNS__TokenResponse)),
            this, SLOT(onTokenDone(TNS__TokenResponse)));
    connect(&m_service, SIGNAL(tokenError(KDSoapMessage)),
            this, SLOT(onTokenError(KDSoapMessage)));
}

void SoapService::getToken(const QString &name, const QString &password)
{
    TNS__Token tokenData;
    tokenData.setName(name);
    tokenData.setPassword(password);
    m_service.asyncToken(tokenData);
}

void SoapService::onTokenDone( const TNS__TokenResponse& parameters )
{
    qDebug() << "SoapService::onTokenDone" << parameters.tokenResult();
    emit tokenDone(parameters.tokenResult());
}

void SoapService::onTokenError( const KDSoapMessage& fault )
{
    qDebug() << "SoapService::onTokenError" << fault.faultAsString();
    emit error(fault.faultAsString());
}
