#ifndef SOAPCLIENTINTERFACE_H
#define SOAPCLIENTINTERFACE_H

#include <QObject>

struct DownloadItem {
    QString name;
    QString description;
    QString url;
};

class SoapClientInterface : public QObject
{
    Q_OBJECT

public:
    // constructor
    explicit SoapClientInterface(QObject *parent = 0);

    // this class methods
    void getToken(const QString &name, const QString &password);
    void getDownloadList(const QString &token);

signals:
    // signals
    void tokenObtained(const QString &token);
    void listObtained(const QList<DownloadItem> &downloadList);
    void error(const QString &message);
};

Q_DECLARE_INTERFACE( SoapClientInterface, "id.go.lemsaneg.lkti.soapclient/1.0")

#endif // SOAPCLIENTINTERFACE_H
