#-------------------------------------------------
#
# Project created by QtCreator 2013-11-27T09:46:34
#
#-------------------------------------------------

QT       += core gui network

TARGET = HelloSOAP
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    soapservice.cpp \
    othersoapclient.cpp

HEADERS  += mainwindow.h \
    soapservice.h \
    othersoapclient.h \
    SoapClientInterface.h

FORMS    += mainwindow.ui
