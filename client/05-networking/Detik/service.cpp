#include "service.h"
#include <QDebug>

#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>

Service::Service(QObject *parent) :
    QObject(parent)
{
}

void Service::test()
{
    qDebug() << "This is a test from qml";
}

void Service::getJSON(const QString &resource)
{
    // 1. build request url
    QString hostUrl = "http://yeyen.blankonlinux.or.id:3003";
    QString requestUrl = hostUrl + "/" + resource;

    qDebug() << "requestUrl: " << requestUrl;

    // 2. build request object
    QNetworkRequest request;
    QUrl url(requestUrl);
    request.setUrl(url);

    // 3. do request
    QNetworkReply * reply = m_networkAccessManager.get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(onGetJSONReplyFinished()));
}

void Service::onGetJSONReplyFinished()
{
    // 4. do process reply
    QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
    QByteArray raw = reply->readAll();
    emit data(raw);
}
