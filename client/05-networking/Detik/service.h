#ifndef SERVICE_H
#define SERVICE_H

#include <QObject>
#include <QNetworkAccessManager>

class Service : public QObject
{
    Q_OBJECT
public:
    explicit Service(QObject *parent = 0);

signals:
    void data(const QString & body);

public slots:
    void test();


    // List of available resources
    // detik/news
    // detik/nasional
    // detik/internasional
    // detik/hot

    void getJSON(const QString & resource);

private slots:
    void onGetJSONReplyFinished();

private:
    QNetworkAccessManager m_networkAccessManager;

};

#endif // SERVICE_H
