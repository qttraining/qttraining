import QtQuick 1.1
import ServicePack 1.0

Rectangle {
    width: 360
    height: 480

    ContentList{
        id: contentList
        anchors.fill: parent
    }

    ListModel{
        id: contentListModel
    }

    FeedList {
        id: feedList
        anchors.fill: parent
    }

    Service{
        id : service
        onData: {
            console.log('data loaded');
            var objectBody = JSON.parse(body);
            var data = objectBody.data;
            var items = data.items;

            contentListModel.clear();

            for(var i = 0; i < items.length; i++) {
                var a = items[i];
                var firstPos = a.description.indexOf('<img src=\"');
                var secondPos = a.description.indexOf('"', 15);
                var image_url = a.description.substring(firstPos + 10, secondPos);
                //console.log(image_url);
                a.image_url = image_url.indexOf('images.detik.com') > 0 ? image_url : 'placeholder.jpg'
                contentListModel.append(items[i]);
            }
        }
    }
}
