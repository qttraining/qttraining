import QtQuick 1.1

Rectangle {
    color: "pink"

    // header part of the page
    Rectangle {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: 60

        Text {
            text: 'Content List'
            font.pixelSize: 20
            anchors.centerIn: parent
        }

        Rectangle{
            height: 2
            color: "black"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }
    }

    // list part of the page
    ListView{
        clip: true
        anchors { top: header.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
        model: contentListModel
        delegate: Rectangle{

            width: ListView.view.width
            height: 100

            Image {
                id: imageItem
                source: model.image_url
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 20
                width: 80
                height: 80
                fillMode: Image.PreserveAspectCrop
                clip: true
            }

            Text{
                id: titleItem
                anchors.left: imageItem.right
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.top: imageItem.top
                font.pixelSize: 14
                text: model.title
                //elide: Text.ElideRight
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            Text{
                id: dateItem
                anchors.left: imageItem.right
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.top: titleItem.bottom
                anchors.topMargin: 5
                font.pixelSize: 12
                text: model.date
                elide: Text.ElideRight
            }

            Rectangle{
                height: 1
                color: "black"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {}
            }
        }
    }
}
