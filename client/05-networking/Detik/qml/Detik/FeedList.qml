import QtQuick 1.1

Rectangle {

    // header part of the page
    Rectangle {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: 60

        Text {
            text: 'Detik News Reader'
            font.pixelSize: 20
            anchors.centerIn: parent
        }

        Rectangle{
            height: 2
            color: "black"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }
    }

    // list part of the page
    ListModel{
        id: mainMenuModel
        ListElement{
            title : "News"
            resource : "detik/news"
        }
        ListElement{
            title : "Nasional"
            resource : "detik/nasional"
        }
        ListElement{
            title : "Internasional"
            resource : "detik/internasional"

        }
        ListElement{
            title : "Hot"
            resource : "detik/hot"
        }
    }

    ListView {
        clip: true
        id: mainMenuList
        anchors { top: header.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
        model: mainMenuModel

        delegate: Rectangle{
            id: listItem
            width: ListView.view.width
            height: 100

            Rectangle{
                height: 1
                color: "black"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
            }

            Text{
                text: model.title
                font.pixelSize: 20
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 20
            }

            MouseArea{
                hoverEnabled: true
                anchors.fill: parent
                onClicked: {
                    feedList.visible = false
                    service.getJSON(model.resource)
                }
                onEntered: {
                    listItem.color = '#d3d3d3'
                }
                onExited: {
                    listItem.color = 'white'
                }
            }
        }
    }
}
