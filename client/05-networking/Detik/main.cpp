#include <QApplication>
#include "qmlapplicationviewer.h"
#include <QtDeclarative>

#include "service.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    qmlRegisterType<Service>("ServicePack", 1, 0, "Service");

    QmlApplicationViewer viewer;
    viewer.addImportPath(QLatin1String("modules"));
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/Detik/main.qml"));
    viewer.showExpanded();

    return app->exec();
}
