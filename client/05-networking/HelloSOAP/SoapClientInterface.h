#ifndef SOAPCLIENTINTERFACE_H
#define SOAPCLIENTINTERFACE_H

#include <QObject>

struct DownloadItem {
    QString name;
    QString description;
    QString url;
};

class SoapClientInterface : public QObject
{
    Q_OBJECT

public:
    // constructor
    SoapClientInterface(QObject *parent) : QObject(parent){}

    // this class methods
    virtual void getToken(const QString &name, const QString &password) = 0;
    virtual void getDownloadList(const QString &token) = 0;

signals:
    // signals
    void tokenObtained(const QString &token);
    void listObtained(const QList<DownloadItem> &downloadList);
    void error(const QString &message);
};

Q_DECLARE_INTERFACE( SoapClientInterface, "id.go.lemsaneg.lkti.soapclient/1.0")

#endif // SOAPCLIENTINTERFACE_H
