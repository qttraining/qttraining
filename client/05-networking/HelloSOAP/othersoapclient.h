#ifndef OTHERSOAPCLIENT_H
#define OTHERSOAPCLIENT_H

#include <QObject>

// 1
// Include generated wsdl file
#include "wsdl_Service.h"

// 2
// Add using namespace Service
using namespace Service;


class OtherSoapClient : public QObject
{
    Q_OBJECT

public:
    // both functions and other data type
    explicit OtherSoapClient(QObject *parent = 0);

    // this class methods
    void getToken(const QString &name, const QString &password);
    void getDownloadList(const QString &token);

signals:
    // related tp class methods
    void tokenObtained(const QString &token);
    void listObtained(const QList<TNS__DownloadItem> &downloadList);
    void error(const QString &message);
    
private slots:
    // functions we will use to listen signals from SoapService
    void onTokenDone( const TNS__TokenResponse& parameters );
    void onTokenError( const KDSoapMessage& fault );
    void onDownloadDone( const TNS__DownloadResponse& parameters );
    void onDownloadError( const KDSoapMessage& fault );

private:
    // class we used to do its job
    ServiceSoap m_service;
};

#endif // OTHERSOAPCLIENT_H
