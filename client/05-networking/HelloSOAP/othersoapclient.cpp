#include "othersoapclient.h"

#include <QDebug>

OtherSoapClient::OtherSoapClient(QObject *parent) :
    QObject(parent)
{
    m_service.setEndPoint(QLatin1String("http://jaegers.azurewebsites.net/Service.asmx"));
    m_service.setSoapVersion(KDSoapClientInterface::SOAP1_2);

    connect(&m_service, SIGNAL(tokenDone(TNS__TokenResponse)),
            this, SLOT(onTokenDone(TNS__TokenResponse)));
    connect(&m_service, SIGNAL(tokenError(KDSoapMessage)),
            this, SLOT(onTokenError(KDSoapMessage)));
    connect(&m_service, SIGNAL(downloadDone(TNS__DownloadResponse)),
            this, SLOT(onDownloadDone(TNS__DownloadResponse)));
    connect(&m_service, SIGNAL(downloadError(KDSoapMessage)),
            this, SLOT(onDownloadError(KDSoapMessage)));
}

void OtherSoapClient::getToken(const QString &name, const QString &password)
{
    TNS__Token tokenData;
    tokenData.setName(name);
    tokenData.setPassword(password);
    m_service.asyncToken(tokenData);
}

void OtherSoapClient::getDownloadList(const QString &token)
{
    TNS__Download downloadData;
    downloadData.setToken(token);
    m_service.asyncDownload(downloadData);
}

void OtherSoapClient::onTokenDone(const TNS__TokenResponse &parameters)
{
    qDebug() << "OtherSoapClient::onTokenDone" << parameters.tokenResult();
    emit tokenObtained(parameters.tokenResult());
}

void OtherSoapClient::onTokenError(const KDSoapMessage &fault)
{
    emit error(fault.faultAsString());
}

void OtherSoapClient::onDownloadDone(const TNS__DownloadResponse &parameters)
{
    emit listObtained(parameters.downloadResult().downloadItem());
}

void OtherSoapClient::onDownloadError(const KDSoapMessage &fault)
{
    emit error(fault.faultAsString());
}
