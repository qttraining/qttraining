#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&m_soapClient, SIGNAL(tokenObtained(QString)),
            this, SLOT(onTokenObtained(QString)));
    connect(&m_soapClient, SIGNAL(listObtained(QList<TNS__DownloadItem>)),
            this, SLOT(onListObtained(QList<TNS__DownloadItem>)));
    connect(&m_soapClient, SIGNAL(error(QString)),
            this, SLOT(onError(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // 7
    // get name and password
    QString name = ui->lineEditName->text();
    QString password = ui->lineEditPassword->text();
    m_soapClient.getToken(name, password);
}

void MainWindow::onTokenObtained(const QString &token)
{
    qDebug() << "MainWindow::onTokenObtained" << token;
    m_token = token;
    ui->label->setText(token);
}

void MainWindow::onListObtained(const QList<TNS__DownloadItem> &downloadList)
{
    for (int i = 0; i < downloadList.count(); i++) {
        ui->listWidget->addItem(downloadList[i].url());
    }
}

void MainWindow::onError(const QString &token)
{

}

void MainWindow::on_pushButtonDownload_clicked()
{
    m_soapClient.getDownloadList(m_token);
}
