
# Qt Training

Qt training materials and codes

* **client** - Qt codes
* **md** - Markdown files for notes
* **ppt** - PPT and PDF for presentation
* **server** - Server code: ASP.NET REST and SOAP service, Node.js file server (upload and static file server)
* **vendor** - Third party libraries: KDSoap (Qt-oriented SOAP library)

### Team

Prepared by

* Nurul Arif Setiawan <n.arif.setiawan@gmail.com>
* Dhi Aurrahman <dio.rahman@gmail.com>
