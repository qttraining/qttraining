var fs = require('fs');
var path = require('path');
var express = require('express');

var uploadPath = __dirname + '/public';

var app = express();
app.use(express.bodyParser({ keepExtensions: true, uploadDir: uploadPath}));
app.use(express.cookieParser());
app.use(express.static(__dirname + '/public'));

var port = process.env.PORT || 3003;

app.get('/', function(req, res) {
  res.send('file storage');
});

app.post('/file', function(req, res) {

  var file = req.files.file;
  
  try {

    var dirname = path.dirname(file.path);
    var newPath = dirname + '\\' + file.name;
    console.log('accept file', newPath);
    
    if (!fs.existsSync(newPath)) {  
      fs.renameSync(file.path,newPath);
      res.send({status:'OK', message: 'File stored in server', path: file.name});
    }
    else {
      fs.unlinkSync(newPath)
      fs.renameSync(file.path,newPath);
      res.send({status:'OK', message: 'File already exists in server and overwritten', path: file.name});
    }

  }
  catch (err) {
    res.send({status:'Error', message: err.message});
  }
});

app.listen(port);
console.log('filestorage is running on port : ' + port);
