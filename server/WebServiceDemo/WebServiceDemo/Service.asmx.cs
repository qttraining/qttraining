﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Xml;
using RandomStringGenerator;

namespace WebServiceDemo
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        RandomStringGenerator.RandomStringGenerator RSG = new RandomStringGenerator.RandomStringGenerator();

        public enum FaultCode
        {
            Client = 0,
            Server = 1
        }

        [WebMethod]
        public List<DownloadItem> Download(String token)
        {
            if (token.Length > 30)
            {
                List<DownloadItem> all = new List<DownloadItem>();
                DownloadItem one = new DownloadItem {
                    name = "RSA paper",
                    description = "RSA paper",
                    url = "http://people.csail.mit.edu/rivest/Rsapaper.pdf"
                };
                DownloadItem two = new DownloadItem
                {
                    name = "AES",
                    description = "AES paper",
                    url = "http://csrc.nist.gov/archive/aes/rijndael/Rijndael-ammended.pdf"
                };
                DownloadItem three = new DownloadItem
                {
                    name = "DES",
                    description = "DES slides",
                    url = "http://www-math.ucdenver.edu/~wcherowi/courses/m5410/des.pdf"
                };

                all.Add(one);
                all.Add(two);
                all.Add(three);

                return all;
            }
            else
            {
                throw RaiseException("GetException", "Service", "Invalid Token",
                      "1", "Download", FaultCode.Client);
            }
        }

        [WebMethod]
        public List<UploadItem> Upload(String token)
        {
            if (token.Length > 30)
            {
                List<UploadItem> all = new List<UploadItem>();
                UploadItem one = new UploadItem
                {
                    name = "File 1",
                    description = "Description of file 1",
                    url = "Url of file 1"
                };
                UploadItem two = new UploadItem
                {
                    name = "File 2",
                    description = "Description of file 2",
                    url = "Url of file 2"
                };
                UploadItem three = new UploadItem
                {
                    name = "File 3",
                    description = "Description of file 3",
                    url = "Url of file 3"
                };

                all.Add(one);
                all.Add(two);
                all.Add(three);

                return all;
            }
            else
            {
                throw RaiseException("GetException", "Service", "Invalid Token",
                      "1", "Upload", FaultCode.Client);
            }
        }

        [WebMethod]
        public String Token(String name, String password)
        {
            if (name == "train" && password == "tr41n")
            {
                System.Threading.Thread.Sleep(100);
                RSG.UseSpecialCharacters = false;
                return RSG.Generate(32);
            }
            else
            {
                throw RaiseException("GetException", "Service", "Invalid Credentials",
                      "1", "Token", FaultCode.Client);
            }
        }

        //Creates the SoapException object with all the error details
        public SoapException RaiseException(string uri, string webServiceNamespace,
                                        string errorMessage,
                                        string errorNumber,
                                        string errorSource,
                                        FaultCode code)
        {
            XmlQualifiedName faultCodeLocation = null;
            //Identify the location of the FaultCode
            switch (code)
            {
                case FaultCode.Client:
                    faultCodeLocation = SoapException.ClientFaultCode;
                    break;
                case FaultCode.Server:
                    faultCodeLocation = SoapException.ServerFaultCode;
                    break;
            }

            XmlDocument xmlDoc = new XmlDocument();
            //Create the Detail node
            XmlNode rootNode = xmlDoc.CreateNode(XmlNodeType.Element,
                                          SoapException.DetailElementName.Name,
                                          SoapException.DetailElementName.Namespace);
            //Build specific details for the SoapException
            //Add first child of detail XML element.
            XmlNode errorNode = xmlDoc.CreateNode(XmlNodeType.Element, "Error",
                                                  webServiceNamespace);
            //Create and set the value for the ErrorNumber node
            XmlNode errorNumberNode =
              xmlDoc.CreateNode(XmlNodeType.Element, "ErrorNumber",
                                webServiceNamespace);
            errorNumberNode.InnerText = errorNumber;
            //Create and set the value for the ErrorMessage node
            XmlNode errorMessageNode = xmlDoc.CreateNode(XmlNodeType.Element,
                                                        "ErrorMessage",
                                                        webServiceNamespace);
            errorMessageNode.InnerText = errorMessage;
            //Create and set the value for the ErrorSource node
            XmlNode errorSourceNode =
              xmlDoc.CreateNode(XmlNodeType.Element, "ErrorSource",
                                webServiceNamespace);
            errorSourceNode.InnerText = errorSource;
            //Append the Error child element nodes to the root detail node.
            errorNode.AppendChild(errorNumberNode);
            errorNode.AppendChild(errorMessageNode);
            errorNode.AppendChild(errorSourceNode);
            //Append the Detail node to the root node
            rootNode.AppendChild(errorNode);
            //Construct the exception
            SoapException soapEx = new SoapException(errorMessage,
                                                     faultCodeLocation, uri,
                                                     rootNode);
            //Raise the exception  back to the caller
            return soapEx;
        }
    }

}