
This is folder for server application

* Metadata Server is ASP.NET web service application which can serve both SOAP and REST
* File server application is Node.js expressjs app for both download and upload files 

Note that you need Visual Studio 2012 to run ASP.NET service or at least [IIS Express](http://www.iis.net/learn/extensions/introduction-to-iis-express/iis-80-express-readme)

To run Node.js server, you must [install Node.js](http://nodejs.org/download/) first.

Some commands for tests:

```
// Multipart POST using curl
curl -F 'file=@C:\\Kertas\\bot.jpg' https://localhost:3003/file'
```

## ASP.NET Server

#### url
http://jaegers.azurewebsites.net/Service.asmx

nam: train
password: tr41n
