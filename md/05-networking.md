# Network Programming

Some references:

http://qt-project.org/doc/qt-4.8/network-programming.html

## HTTP

Use of QNetworkAccessManager, QNetworkRequest and QNetworkReply

### REST

Representational State Transfer (REST) is an architectural style that abstracts the architectural elements within a distributed hypermedia system.

### SOAP

SOAP, originally defined as Simple Object Access Protocol, is a protocol specification for exchanging structured information in the implementation of Web Services in computer networks.

### Downloader

Downloader example

### Uploader

Uploader example

## Application ClientApp state

* QML with C++
* REST client
* SOAP client
* ListView
* Downloader client
* Uploader client

