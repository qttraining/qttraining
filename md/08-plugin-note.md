# Plugin

## Create new Project
* C++ library
* shared
* module: network and core

## Prepare KDSOAP
* copy service.wsdl
* copy kdsoap.pri

## Plugin class

### Interface 

* implement empty constructor
* all methods are virtual
* add Q_DECLARE_INTERFACE(ClassName, identifierString)

```
struct DownloadItem {
    QString name;
    QString description;
    QString url;
};

class SoapClientInterface : public QObject
{
    Q_OBJECT

public:
    // empty constructor
    SoapClientInterface(QObject *parent) : QObject(parent){}

    // virtual methods
    virtual void getToken(const QString &name, const QString &password) = 0;
    virtual void getDownloadList(const QString &token) = 0;

signals:
    // signals
    void tokenObtained(const QString &token);
    void listObtained(const QList<DownloadItem> &downloadList);
    void error(const QString &message);
};

Q_DECLARE_INTERFACE( SoapClientInterface, "id.go.lemsaneg.lkti.soapclient/1.0")
```

###Plugin header

* inherit from interface
* add Q_INTERFACES(SoapClientInterface)

```
#include <QtPlugin>

// inherit class from interface 
class SoapPlugin : public SoapClientInterface
{
    Q_OBJECT

    // declare interface used in plugin
    Q_INTERFACES(SoapClientInterface)

public:
    // constructor
    explicit SoapPlugin(QObject *parent = 0);

    // this class methods
    // same signature with interface declaration
    void getToken(const QString &name, const QString &password);
    void getDownloadList(const QString &token);

    // use signals from interface

private slots:
    // functions we will use to listen signals from SoapService
    void onTokenDone( const TNS__TokenResponse& parameters );
    void onTokenError( const KDSoapMessage& fault );
    void onDownloadDone( const TNS__DownloadResponse& parameters );
    void onDownloadError( const KDSoapMessage& fault );

private:
    // class we used to do its job
    ServiceSoap m_service;
};
```

###Plugin source

* add Q_EXPORT_PLUGIN2(SoapPlugin, SoapPlugin)
```
SoapPlugin::SoapPlugin(QObject *parent) :
    SoapClientInterface(parent)
{
    m_service.setEndPoint(QLatin1String("http://jaegers.azurewebsites.net/Service.asmx"));
    m_service.setSoapVersion(KDSoapClientInterface::SOAP1_2);

    connect(&m_service, SIGNAL(tokenDone(TNS__TokenResponse)),
            this, SLOT(onTokenDone(TNS__TokenResponse)));
    connect(&m_service, SIGNAL(tokenError(KDSoapMessage)),
            this, SLOT(onTokenError(KDSoapMessage)));
    connect(&m_service, SIGNAL(downloadDone(TNS__DownloadResponse)),
            this, SLOT(onDownloadDone(TNS__DownloadResponse)));
    connect(&m_service, SIGNAL(downloadError(KDSoapMessage)),
            this, SLOT(onDownloadError(KDSoapMessage)));
}

// add plugin export
Q_EXPORT_PLUGIN2(SoapPlugin, SoapPlugin)

void SoapPlugin::getToken(const QString &name, const QString &password)
{
    TNS__Token tokenData;
    tokenData.setName(name);
    tokenData.setPassword(password);
    m_service.asyncToken(tokenData);
}

void SoapPlugin::getDownloadList(const QString &token)
{
    TNS__Download downloadData;
    downloadData.setToken(token);
    m_service.asyncDownload(downloadData);
}

void SoapPlugin::onTokenDone(const TNS__TokenResponse &parameters)
{
    qDebug() << "OtherSoapClient::onTokenDone" << parameters.tokenResult();
    emit tokenObtained(parameters.tokenResult());
}

void SoapPlugin::onTokenError(const KDSoapMessage &fault)
{
    emit error(fault.faultAsString());
}

void SoapPlugin::onDownloadDone(const TNS__DownloadResponse &parameters)
{
    QList<DownloadItem> downloadList;

    QList<TNS__DownloadItem> responseList = parameters.downloadResult().downloadItem();
    for (int i = 0; i < responseList.size(); i++)
    {
        DownloadItem item;
        item.name = responseList[i].name();
        item.description = responseList[i].description();
        item.url = responseList[i].url();
        downloadList.append(item);
    }

    emit listObtained(downloadList);
}

void SoapPlugin::onDownloadError(const KDSoapMessage &fault)
{
    emit error(fault.faultAsString());
}
```
