# Qt Training

## Introduction

Qt is an application development framework based on C++. Traditionally, C++ is the major programming language used to develop with Qt. Since 2011, Qt has been supporting script-based declarative programming with QML.

In this training, we will work to create one functional application from beginning. Starting with setting up Qt project, link required libraries and start adding application components until completion. We will uncover Qt SQK libraries for handling collections, networking, multithreading and many others. 

## Modules

* Fundamentals of Qt
* Application with QML
* Networking and Consuming Service
* Working with Libraries
* Qt Webkit

## Prerequisites

A solid understanding of the basics of C++. 

## Tools

* **QtSDK.** one package with all the tools you need for the creation of applications for desktop platforms such as Microsoft Windows, Mac OS X, and Linux.
* **QtCreator.** a cross-platform integrated development environment (IDE). Note, that the Qt Creator package does not contain the Qt framework itself. You can either configure it to use with a version of Qt already installed on your machine or download a new version of Qt separately.

## Installation

QtSDK and QtCreator installations

* `QtSdk-online-win-x86-v1_2_1.exe` provide simple installation process of QtSQK 4.8.1 with QtCreator.
* Steps to install Qt using latest installer maybe needed

TODO :: Install latest Qt