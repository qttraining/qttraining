# Develop Application with QML

Some references :

http://www.ics.com/technologies/qt-quick-qml-application-development
http://qt-project.org/doc/qt-5.0/qtquick/qtquick-index.html
http://qt-project.org/doc/qt-5.0/qtquick/qtquick-qmltypereference.html
http://qt-project.org/doc/qt-4.8/qtquick.html

The Qt Quick module is the standard library for writing QML applications. QML is declarative scripting language use in Qt Quick.

## QML Visual Types

Item 
Rectangle
Image
Text

TODO :: example

## User input

TODO :: example

## State, Transition and Animation

TODO :: example

## MVC

ListView and ListModel

TODO :: example

## QML Signals and Handler

TODO :: example

## Extending QML with C++

TODO :: example

## Application ClientApp state

* UI layout
* Buttons
* Transitions

