# Fundamentals of Qt

Reference 
http://qt-project.org/doc/qt-4.8/qt-basic-concepts.html
http://qt-project.org/doc/qt-4.8/how-to-learn-qt.html
http://harmattan-dev.nokia.com/docs/platform-api-reference/xml/daily-docs/libqt4/signalsandslots.html
http://mayaposch.wordpress.com/2011/11/01/how-to-really-truly-use-qthreads-the-full-explanation/

## Programming With Qt

### Basic types

Standard C++: char, int, float, double
Struct and Arrays
Container: QList, QMap, QPair
QString
QVariant
...

### Qt Object Model

#### QObject: base for all classes in Qt

* Memory Management
* Introspection (runtime identification of object types)
* Event handling

#### QObject parent-child relationship

QObject may declare parent when created.

#### QObject memory management

Ownership transferred to parent.
Parent delete children.
No garbage collection

#### Meta-Object System

Extends C++ with dynamic features.

* Mechanism to access any functions in the class
* Class information
* Dynamic property

## Signal and Slot

* Observer pattern
* Type-safe callbacks
* Many-to-many relationship
* Implemented in QObject

TODO :: Example application to demonstrate QObject concepts and Signal and Slot

## Developing GUI Application with Qt

### Widgets 

Widget style UI

### GraphicsView

Graphics View framework for 2D graphics application

### QML

Qt Quick UI 

### Webkit

HTML based application

TODO :: Example application for each GUI options

## Qt Model/View Programming

Introduction to MVC pattern in Qt

TODO :: Example of MVC pattern in QWidget

## Embedded Platform with Qt (might be interesting)

Qt on beagleboard?
